package ru.gryazev.tm.service;

import ru.gryazev.tm.domain.Project;
import ru.gryazev.tm.domain.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.InputUtils;
import java.io.BufferedReader;
import java.io.IOException;

public class TaskService {
    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;

    public TaskService(ProjectRepository projectRepository) {
        this.taskRepository = new TaskRepository();
        this.projectRepository = projectRepository;
    }

    public void create(BufferedReader in) throws IOException {
        System.out.println("[TASK CREATE]");
        int id = InputUtils.inputNumber(in, "project id");
        Project p = projectRepository.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            String name;
            do
                System.out.println("Enter task name:");
            while ("".equals(name = in.readLine()));
            taskRepository.add(new Task(name, id));
            System.out.println("[OK]");
        }
    }

    public void view(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");
        Project p = projectRepository.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            System.out.println("Enter task name:");
            String name = in.readLine();
            taskRepository.getTaskByName(name).stream().forEach(o ->
                    System.out.println(String.format("Name: %s\nDetails: %s", o.getName(), o.getDetails())));
        }
    }

    public void list(BufferedReader in) throws IOException{
        taskRepository.getTasks().stream().forEach(System.out::println);
    }

    public void edit(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        Project p = projectRepository.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            id = InputUtils.inputNumber(in, "task id");
            Task t = taskRepository.getTaskById(id);
            if (t == null)
                System.out.println("Task not found");
            else {
                String name;
                System.out.println("[TASK EDIT]");
                do
                    System.out.println("Enter new name:");
                while ("".equals(name = in.readLine()));
                t.setName(name);
                System.out.println("[OK]");
            }
        }
    }

    public void remove(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        Project p = projectRepository.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            id = InputUtils.inputNumber(in, "task id");
            if (taskRepository.remove(id))
                System.out.println("[DELETED]");
            else
                System.out.println("Task not found");
        }
    }
}