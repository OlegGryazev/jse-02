package ru.gryazev.tm.service;

import ru.gryazev.tm.domain.Project;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.util.InputUtils;
import java.io.BufferedReader;
import java.io.IOException;

public class ProjectService{
    private ProjectRepository projectRepository;

    public ProjectService() {
        this.projectRepository = new ProjectRepository();
    }

    public void create(BufferedReader in) throws IOException {
        String name;
        System.out.println("[PROJECT CREATE]");
        do
            System.out.println("Enter name:");
        while ("".equals(name = in.readLine()));
        projectRepository.add(new Project(name));
        System.out.println("[OK]");
    }

    public void view(BufferedReader in) throws IOException {
        String result;
        System.out.println("Enter name:");
        String name = in.readLine();
        projectRepository.getProjectByName(name).stream().forEach(o ->
                System.out.println(String.format("Name: %s\nDetails: %s", o.getName(), o.getDetails())));
    }

    public void list() {
        projectRepository.getProjects().stream().forEach(System.out::println);
    }

    public void edit(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        Project p = projectRepository.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            String name;
            System.out.println("[PROJECT EDIT]");
            do
                System.out.println("Enter name:");
            while ("".equals(name = in.readLine()));
            p.setName(name);
            System.out.println("[OK]");
        }
    }

    public void remove(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        if (projectRepository.remove(id))
            System.out.println("[DELETED]");
        else
            System.out.println("Project not found");
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }
}
