package ru.gryazev.tm.domain;

public class Task {
    private static int maxId = 0;
    private int id;
    private int projectId;
    private String name;
    private String details;

    public Task(String name, int projectId) {
        this.name = name;
        this.id = maxId++;
        this.projectId = projectId;
        details = "Some details about task";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }
}
