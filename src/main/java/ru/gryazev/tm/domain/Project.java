package ru.gryazev.tm.domain;

public class Project {
    private static int maxId = 0;
    private int id;
    private String name;
    private String details;

    public Project(String name) {
        this.name = name;
        this.details = "Some details about project";
        id = maxId++;
    }

    public String getName() {
        return name;
    }

    public String getDetails() {
        return details;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public static int getMaxId() {
        return maxId;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + ". " + name;
    }
}
