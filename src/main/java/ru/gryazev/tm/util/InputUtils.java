package ru.gryazev.tm.util;

import java.io.BufferedReader;
import java.io.IOException;


public class InputUtils {
    public static int inputNumber(BufferedReader in, String source) {
        int id;
        System.out.println(String.format("Enter %s:", source));
        try {
            id = Integer.parseInt(in.readLine());
        } catch (NumberFormatException | IOException e) {
            System.out.println(String.format("Entered %s is incorrect.", source));
            return -1;
        }
        return id;
    }

}
