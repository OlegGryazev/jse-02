#### Project Manager v1.0

###### Software requirements:
    JDK 8
    Apache Maven 3.6.3
    
###### Technology stack:
    Maven
    Java SE
    
###### Developer:
    Gryazev Oleg
    email: gryazev77@gmail.com
    
###### Build:
    mvn clean compile assembly:single
    
###### Run:
    java -jar target/ProjectManager.jar